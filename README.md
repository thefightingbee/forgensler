# ForGensler

Technical Doc Example

# Gangneung AM2 
Attraction Manager 2 for gangneung set-up. 
All communications are based on ZMQ and JSON. 
*todo: Protobuf

## Running Teabar Proxy AM2 
 - runs on every teabar pc, used to communicate internally and with CMS_router
 - projects\yeosu_teabar_am2\builds\x64\Release\yeosu_teabar_am2_heartbeatmonitor.exe
 ```
 // using default ports: xsub:5550, xpub:5551, capture:5552, router:192.168.0.1:5590, ab
 // ./[executable.exe] [xsub port] [xpub port] [capture port] [router ip] [table name]
 // ./[executable.exe] [router ip] [table name]
 ./yeosu_teabar_am2_heartbeatmonitor.exe 192.168.0.1 AB

 ```
## Running Teabar CMS Router
 - runs on CMS server pc, used to route message between tables and CMS unity tablet.
 - projects\yeosu_teabar_am2\builds\x64\Release\yeosu_teabar_am2_cms_router.exe
 
 ```
 // using default ports: router *:5590
 ./yeosu_teabar_am2_cms_router.exe

 // using custom router port 
 ./yeosu_teabar_am2_cms_router.exe 1234
 
 ```
## General Data flow Attraction Manager
```mermaid
flowchart LR;
  subgraph Attraction Manager;
  HeartbeatMonitor<-->SystemControl
  end
  subgraph Modular Software
  Unity<-->HeartbeatMonitor;
  TeaTracker<-->HeartbeatMonitor;
  CMS<-->HeartbeatMonitor;
  end
  subgraph Attraction Dashboard
  AttractionDashboard
  end
  SystemControl<-->AttractionDashboard;
  AttractionDashboard-->Projector_0
  AttractionDashboard-->Projector_1
  AttractionDashboard-->Projector_2
  AttractionDashboard-->Projector_N
```

## Teabar ZMQ Data Flow and Ports
```mermaid
flowchart LR;
  subgraph CMS_ROUTER
  Router:5590
  end

  subgraph Teabar PC X10
    subgraph AM2
      subgraph Proxy;
      XSUB:5550<-->proxy
      proxy
      XPUB:5551<-->proxy
      end
      subgraph SystemControl
      proxy<-->dealer_N
      dealer_N<-->|connect:5590|Router:5590
      end
    end
    
    subgraph Unity
    unityPUB-->|connect:5550|XSUB:5550;
    unitySUB-->|connect:5551|XPUB:5551
    end

    subgraph Darknet
    darknetPUB-->|connect:5550|XSUB:5550
    darknetSUB-->|connect:5551|XPUB:5551
    end
  end
 

  subgraph CMS_UNITY
  dealer_CMSUNITY<-->|connect:5590|Router:5590
  end
```
